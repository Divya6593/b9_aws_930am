#### Session Video:
       https://drive.google.com/file/d/1bcpcDrJ67l16P9TJQ4e1N9gpIWNQkixt/view?usp=sharing

#### AWS Basics

1. AWS Personal Health

```
AWS Personal Health Dashboard is a service provided by Amazon Web Services (AWS) that helps AWS customers stay informed about the status of their resources and services in the AWS Cloud. It provides personalized and real-time information about the health of an individual's AWS environment.

The AWS Personal Health Dashboard displays information about:

Events: These are operational events that might impact the availability of your resources. Examples include EC2 instance hardware degradation, network connectivity issues, or maintenance events.

Scheduled Maintenance: AWS informs you about upcoming maintenance events that could affect your resources. This allows you to plan and take appropriate actions to minimize any potential impact on your applications.

Other Notifications: The dashboard might also provide general notifications and updates related to the AWS services you are using.

The dashboard is designed to make it easier for users to quickly identify and understand the impact of events on their resources. It categorizes events based on their severity, allowing users to prioritize their responses. For example, critical events might require immediate attention, while informational events might be of lower priority.

The information on the AWS Personal Health Dashboard can be accessed through the AWS Management Console or programmatically using AWS APIs. It's a useful tool for maintaining the health and availability of your AWS resources and services, helping you to respond effectively to any issues that may arise.

```
2. AWS Service Health 

```
AWS Service Health is a feature provided by Amazon Web Services (AWS) that offers customers real-time information about the operational status of various AWS services and regions. It helps customers stay informed about the current health and performance of AWS services, enabling them to make informed decisions and take appropriate actions to manage their applications and workloads.

The AWS Service Health Dashboard provides information about:

Service Outages: It alerts customers about service disruptions or outages that might be impacting specific AWS services. These disruptions could be due to technical issues, maintenance, or other factors.

Service Degradations: The dashboard highlights situations where a service might be experiencing degraded performance, even if it's not a complete outage. This information helps customers understand if their applications might be affected.

Resolved Issues: AWS updates the dashboard with information about issues that have been resolved. This helps customers track the progress of any ongoing incidents.

Upcoming Maintenance: AWS informs customers about planned maintenance events that might impact the availability of their resources. This allows customers to plan their activities accordingly.

Historical Data: The dashboard also provides historical information about the health of AWS services. This can be useful for trend analysis and understanding the overall reliability of various services over time.

Customers can access the AWS Service Health Dashboard through the AWS Management Console. The dashboard provides a visual representation of the status of AWS services and regions using color-coded indicators. Different colors indicate different levels of severity, ranging from operational to service disruptions.

By regularly monitoring the AWS Service Health Dashboard, customers can proactively respond to any issues that might impact their applications and take appropriate measures to ensure the reliability and availability of their AWS resources.

```
3. Amazon EC2 Overivew

```
Amazon Elastic Compute Cloud (Amazon EC2) is one of the core services provided by Amazon Web Services (AWS) that allows you to easily provision and manage virtual servers in the cloud. It provides scalable compute capacity, enabling you to quickly launch and scale instances to meet your application's needs. Here's an overview of Amazon EC2:

Virtual Servers (Instances):
Amazon EC2 allows you to create and manage virtual servers known as "instances." These instances can run various operating systems and can be customized with different amounts of CPU, memory, storage, and networking resources based on your application's requirements.

Scalability:
EC2 instances can be easily scaled up or down to handle varying workloads. You can launch multiple instances when demand increases and terminate them when demand decreases, helping you optimize costs and performance.

Instance Types:
Amazon EC2 offers a wide range of instance types optimized for various use cases, such as general-purpose computing, memory-intensive tasks, compute-intensive workloads, and more. Each instance type has specific combinations of CPU, memory, storage, and network capabilities.

Amazon Machine Images (AMIs):
An AMI is a pre-configured template that contains the necessary software, operating system, and settings to launch an instance. AWS provides a variety of pre-built AMIs, and you can also create your own custom AMIs.

Elastic IP Addresses:
EC2 instances are assigned dynamic IP addresses by default, but you can allocate Elastic IP addresses to instances to provide a static IP that doesn't change even if the instance is stopped and restarted.

Security Groups and Network Security:
EC2 instances can be placed within virtual networks called Virtual Private Clouds (VPCs). Security Groups act as firewalls that control inbound and outbound traffic to instances, enhancing network security.

Storage Options:
EC2 instances can be attached to various types of storage, including Amazon Elastic Block Store (EBS) volumes for persistent block storage and instance store volumes for temporary storage.

Auto Scaling:
EC2 Auto Scaling enables you to automatically adjust the number of instances in a group based on factors like demand, ensuring that your application scales seamlessly.

Load Balancing:
Amazon EC2 instances can be deployed behind load balancers to distribute incoming traffic across multiple instances, enhancing availability and fault tolerance.

Managed Services Integration:
EC2 instances can be integrated with various managed AWS services such as Amazon RDS (Relational Database Service), Amazon S3 (Simple Storage Service), AWS Lambda, and more.

Pay-as-You-Go Pricing:
EC2 follows a pay-as-you-go pricing model where you are billed only for the compute capacity you use. Different instance types have different pricing levels based on their capabilities.

Amazon EC2 is a fundamental building block for running a wide range of applications, from web hosting and application development to large-scale data processing and machine learning workloads. It offers flexibility, scalability, and a variety of configuration options to meet the needs of different use cases.
```

